# https://www.ebay.co.uk/sch/i.html?_from=R40&_nkw=thinkpad+x200&_sop=2
# https://www.ebay.co.uk/sch/i.html?_from=R40&_nkw=thinkpad+x220&_sop=2
import requests
from bs4 import BeautifulSoup
import smtplib


def productSearchLink(link, min, max, msg):
    r = requests.get(link)
    soup = BeautifulSoup(r.content, 'html.parser')
    title = soup.select(".s-item--watch-at-corner .s-item__title")
    price = soup.select(".s-item--watch-at-corner .s-item__price")
    link = soup.select(".s-item--watch-at-corner .s-item__link")

    time = soup.select(".s-item--watch-at-corner .s-item__time-left")
    for i in range(len(price)):
        if (float(price[i].text.strip()[1:]) >= min and float(price[i].text.strip()[1:]) <= max):
            server = smtplib.SMTP('smtp.gmail.com', 587)
            server.starttls()
            server.login(sender, password)
            try:
                server.sendmail(
                    sender, receiver, f"Subject: {msg,price[i].text.strip()[1:]}\n\n{title[i].text.strip().replace('®','').replace('™','')}\n{price[i].text.strip()[1:]}\n{time[i].text.strip()}\n{link[i]['href']}")
            except:
                server.sendmail(
                    sender, receiver, f"Subject: {msg,price[i].text.strip()[1:]}\n\n{title[i].text.strip().replace('®','').replace('™','')}\n{price[i].text.strip()[1:]}\n{link[i]['href']}")


productSearchLink(
    "https://www.ebay.co.uk/sch/i.html?_from=R40&_nkw=thinkpad+x200&_sop=2", 10, 100, "200")
productSearchLink(
    "https://www.ebay.co.uk/sch/i.html?_from=R40&_nkw=thinkpad+x220&_sop=2", 10, 100, "220")
